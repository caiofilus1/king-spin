const electron = require('electron');
const cv = require('opencv4nodejs');
const robot = require("robotjs");

export default {
    data() {
        return {
            screen: {size: {width: 0, height: 0}},
            normalizeScale: 1,
            logResult: true,
            threshold: 90,
            scales: [ 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.9, 0.95, 1, 1.05, 1.10, 1.15, 1.20, 1.25, 1.30],
            playAgainLabel: '',
            registerLabel: '',
            log: '',
        }
    },
    methods: {
        automation(canvas) {
            let url = canvas.toDataURL('image/png', 1);
            const screenImg = url.replace('data:image/jpeg;base64', '')
                .replace('data:image/png;base64', '');
            let src = cv.imdecode(Buffer.from(screenImg, 'base64'));
            src = src.rescale(this.screen.scaleFactor);
            src = src.rescale(this.normalizeScale);
            let grayImage = src.bgrToGray();
            let playAgainImg = require('@/assets/PlayAgainButton_720.png');
            let registerImg = require('@/assets/RegisterButton_720.png');

            this.findTemplate(grayImage, playAgainImg, 'Play Again');
            this.findTemplate(grayImage, registerImg, 'Register');
        },
        findTemplate(fullImage, template, label) {
            const base64data = template.replace('data:image/jpeg;base64', '')
                .replace('data:image/png;base64', '');
            let templateImage = cv.imdecode(Buffer.from(base64data, 'base64'));
            templateImage = templateImage.bgrToGray();
            let maxScale;
            let templateResult = {maxLoc: {}, maxVal: 0,};
            for (let scale of this.scales) {
                let resizeTemplate = templateImage.rescale(scale);
                let result = fullImage.matchTemplate(resizeTemplate, cv.TM_CCOEFF_NORMED);
                let points = cv.minMaxLoc(result);
                // console.log(scale);
                if (templateResult.maxVal <= points.maxVal) {
                    maxScale = scale;
                    templateResult = points;
                }
            }
            if (this.logResult) {
                this.printLog(`Result: ${(templateResult.maxVal * 100).toFixed(2)}, Scale: ${maxScale}`);
            }
            let text = `${(100 * templateResult.maxVal).toFixed(2)}%, Scale ${maxScale}`;
            if (label === 'Register') {
                this.registerLabel = text
            } else {
                this.playAgainLabel = text
            }
            if (templateResult.maxVal >= this.threshold * 0.01) {
                templateResult.maxLoc.x = Math.floor(templateResult.maxLoc.x / this.normalizeScale);
                let mousePos = robot.getMousePos();
                let x = Math.floor(templateResult.maxLoc.x * (1 / this.normalizeScale) + (templateImage.cols / 2));
                let y = Math.floor(templateResult.maxLoc.y * (1 / this.normalizeScale) + (templateImage.rows / 2));
                robot.moveMouse(x, y);
                robot.mouseClick();
                robot.moveMouse(mousePos.x, mousePos.y);

                this.scales = [maxScale - 0.15, maxScale - 0.10, maxScale - 0.05, maxScale, maxScale + 0.05, maxScale + 0.10, maxScale + 0.15,];
            }
        },
        printLog() {

            // this.log += msg + '<br>';
            // if (this.log.length > 400) {
            //     this.log = this.log.substring(100, this.log.length - 1)
            // }
        }
    },
    mounted() {
        this.screen = electron.remote.screen.getPrimaryDisplay();
        this.normalizeScale = 720 / (this.screen.scaleFactor * this.screen.size.height);
    }
}