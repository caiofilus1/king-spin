const electron = require('electron');
const path = require('path');
const fs = require('fs');


class Config {
    constructor() {
        this.userDataPath = (electron.app || electron.remote.app).getPath('userData');
        this.jsonPath = path.join(this.userDataPath, 'config.json');
        this.data = {};
        try {
            Object.assign(this, JSON.parse(fs.readFileSync(this.jsonPath)));
        } catch (error) {
            console.log(error);
        }
    }

    assign(result) {
        return Object.assign(result, this.data);
    }

    get(key) {
        return this.data[key];
    }

    set(key, val) {
        this.data[key] = val;
        fs.writeFileSync(this.jsonPath, JSON.stringify(this.data));
    }


}

export default new Config()