import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#0D0D0D',
                secondary: '#8C3737',
                accent: '#DCB068',
                black: '#0D0D0D',
            },
        },
    },
});
