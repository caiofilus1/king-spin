import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from "../views/Main";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Main
    },
];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes
});

export default router
