module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    pluginOptions: {
        electronBuilder: {
            nodeIntegration: true,
            asar: false,
        }
    }
};